package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.SpecialityDto;
import com.project.project_service_api.dao.entity.Speciality;
import com.project.project_service_api.service.SpecialityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class SpecialityController {

    private  final SpecialityService specialityService;

    @GetMapping(value = "/speciality", produces = "application/json")
    public ResponseEntity<List<Speciality>> index(){
        return ResponseEntity.ok(specialityService.getAllSpeciality());
    }

    @GetMapping(value = "/speciality/{id}", produces = "application/json")
    public ResponseEntity<List<Speciality>> getByEstablishment(@PathVariable("id") Long id){
        return ResponseEntity.ok(specialityService.getSpecialityForEstablishment(id));
    }

    @PostMapping(value = "/speciality", produces = "application/json")
    @PreAuthorize("hasAnyAuthority('PERSONAL', 'ADMIN')")
    public ResponseEntity<Speciality> store(@RequestBody SpecialityDto specialityDto){
        return ResponseEntity.ok(specialityService.addSpeciality(specialityDto));
    }
}
