package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.EstablishmentDto;
import com.project.project_service_api.dao.entity.Establishment;
import com.project.project_service_api.service.EstablishmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class EstablishmentController {

    private final EstablishmentService establishmentService;

    @GetMapping(value = "/establishment",produces = "application/json")
    public ResponseEntity<List> index(){
        return ResponseEntity.ok(establishmentService.getAllEstablishment());
    }

    @PostMapping(value = "/establishment",produces = "application/json")
    @PreAuthorize("hasAnyAuthority('PERSONAL', 'ADMIN')")
    public ResponseEntity<Establishment> store(@RequestBody EstablishmentDto establishmentDto){
        return ResponseEntity.ok(establishmentService.addEstablishment(establishmentDto));
    }
}
