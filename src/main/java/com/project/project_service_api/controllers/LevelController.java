package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.LevelDto;
import com.project.project_service_api.dao.entity.Level;
import com.project.project_service_api.service.LevelService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class LevelController {

    private final LevelService levelService;

    @PostMapping(value = "/level", produces = "application/json")
    @PreAuthorize("hasAnyAuthority('PERSONAL', 'ADMIN')")
    public ResponseEntity<Level> store(@RequestBody LevelDto levelDto){
        return ResponseEntity.ok(levelService.addLevel(levelDto));
    }
}
