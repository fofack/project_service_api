package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.PlaningDto;
import com.project.project_service_api.dao.dto.SearchPlaningDto;
import com.project.project_service_api.dao.entity.Planing;
import com.project.project_service_api.service.PlaningService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class PlaningController {

    private final PlaningService planingService;

    @PostMapping(value = "/planing",produces = "application/json")
    @PreAuthorize("hasAnyAuthority('PERSONAL', 'ADMIN')")
    public ResponseEntity<Planing> store(@RequestBody PlaningDto planingDto){
        return ResponseEntity.ok(planingService.addPlaning(planingDto));
    }

    @PostMapping(value = "/planing/week", produces = "application/json")
    public ResponseEntity<List> getPlaningByWeek(@RequestBody SearchPlaningDto searchPlaningDto){
        return ResponseEntity.ok(planingService.getPlaningForWeek(searchPlaningDto));
    }
}
