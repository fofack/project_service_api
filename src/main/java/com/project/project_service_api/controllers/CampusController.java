package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.CampusDto;
import com.project.project_service_api.dao.entity.Campus;
import com.project.project_service_api.service.CampusService;
import com.project.project_service_api.utils.EnvUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class CampusController {

    private final CampusService campusService;

    @GetMapping(value = "/campus", produces ="application/json" )
    public ResponseEntity<List> index(){
        return ResponseEntity.ok(campusService.getAllCampus());
    }

    @PostMapping(value = "/campus", produces = "application/json")
    @PreAuthorize("hasAnyAuthority('PERSONAL', 'ADMIN')")
    public ResponseEntity<Campus> store(@RequestBody CampusDto campusDto){
        return ResponseEntity.ok(campusService.createCampus(campusDto));
    }

    @PutMapping(value = "/campus/{id}", produces = "application/json")
    @PreAuthorize("hasAnyAuthority('PERSONAL', 'ADMIN')")
    public ResponseEntity<Campus> update(@RequestBody CampusDto campusDto, @PathVariable("id") Long id){
        return ResponseEntity.ok(campusService.updateCampus(campusDto, id));
    }
}
