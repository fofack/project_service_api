package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.ResetPasswordDto;
import com.project.project_service_api.dao.dto.StudentDto;
import com.project.project_service_api.dao.entity.Parameter;
import com.project.project_service_api.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class StudentController {

    private final StudentService studentService;

    @PostMapping(value = "/student", produces = "application/json")
    @PreAuthorize("hasAnyAuthority('PERSONAL', 'ADMIN')")
    public ResponseEntity<Parameter> store(@RequestBody StudentDto studentDto){
        return ResponseEntity.ok(studentService.addStudent(studentDto));
    }

}
