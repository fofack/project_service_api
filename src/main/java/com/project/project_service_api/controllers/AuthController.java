package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.ResetPasswordDto;
import com.project.project_service_api.dao.entity.User;
import com.project.project_service_api.service.AuthService;
import com.project.project_service_api.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping(value = "/auth")
public class AuthController {
        private final AuthService authService;
        private final UserService userService;

        @GetMapping(path = "/refreshToken")
        public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
            authService.refreshTokenOfToken(request,response);
        }

        @GetMapping(value = "/profile")
        public ResponseEntity<User> profile(Principal principal){
            return ResponseEntity.ok(userService.findUserByUsername(principal.getName()));
        }

    @PutMapping(value = "/reset/password", produces = "application/json")
    public ResponseEntity<String> resetPassword(@RequestBody ResetPasswordDto resetPasswordDto, Principal principal){
        authService.updatePassword(resetPasswordDto, principal.getName());
        return ResponseEntity.ok("Password changed successfully");
    }
}
