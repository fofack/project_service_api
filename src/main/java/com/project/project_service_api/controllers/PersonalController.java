package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.PersonalDto;
import com.project.project_service_api.dao.dto.PhotoDto;
import com.project.project_service_api.dao.dto.ResetPasswordDto;
import com.project.project_service_api.dao.entity.Parameter;
import com.project.project_service_api.service.PersonalService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.InetAddress;
import java.security.Principal;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class PersonalController {

    @Autowired
    private PersonalService personalService;
    InetAddress ip;
    @GetMapping(value = "/personal/{specialityId}",produces = "application/json")
    public ResponseEntity<List> getPersonalForSpeciality(@PathVariable("specialityId") Long specialityId){
        return ResponseEntity.ok(personalService.getAllPersonalForSpeciality(specialityId));
    }

    @PostMapping(value = "/personal",produces = "application/json")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<Parameter> store(@RequestBody PersonalDto personalDto){
        return ResponseEntity.ok(personalService.addPersonal(personalDto));
    }

    @PostMapping(value = "/personal/photo", consumes = { "multipart/form-data" })
    @PreAuthorize("hasAnyAuthority('PERSONAL', 'ADMIN')")
    public ResponseEntity<String> changePicture (@RequestBody MultipartFile photo, Principal principal) throws IOException {
        return ResponseEntity.ok(personalService.updatePhoto(photo, principal.getName()));
    }
}
