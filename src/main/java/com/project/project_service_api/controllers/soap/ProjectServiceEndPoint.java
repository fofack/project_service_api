package com.project.project_service_api.controllers.soap;

import com.project.project_service_api.dao.dto.PersonalDto;
import com.project.project_service_api.dao.dto.PlaningDto;
import com.project.project_service_api.dao.dto.SearchPlaningDto;
import com.project.project_service_api.dao.dto.StudentDto;
import com.project.project_service_api.dao.entity.*;
import com.project.project_service_api.dao.repository.PlaningRepository;
import com.project.project_service_api.service.*;
import com.project_service.gs_ws.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

@Endpoint
@RequiredArgsConstructor
public class ProjectServiceEndPoint {

    public static final String NAMESPACE_URI = "http://www.project_service.com/uds-ws";

    private final CampusService campusService;
    private final StudentService studentService;
    private final PersonalService personalService;
    private final EstablishmentService establishmentService;
    private final SpecialityService specialityService;
    private final PlaningService planingService;

    //Liste des campus
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllCampusRequest")
    @ResponsePayload
    public GetAllCampusResponse getAllCampus(@RequestPayload GetAllCampusRequest request) {
        GetAllCampusResponse response = new GetAllCampusResponse();
        List<CampusType> campusTypeList = new ArrayList<>();
        List<Campus> campusList = campusService.getAllCampus();
        for (Campus campus : campusList) {
            CampusType campusType = new CampusType();
            BeanUtils.copyProperties(campus, campusType);
            campusTypeList.add(campusType);
        }
        response.getCampusType().addAll(campusTypeList);

        return response;

    }

    //Liste des étudiants
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllStudentRequest")
    @ResponsePayload
    public GetAllStudentResponse getAllStudent(@RequestPayload GetAllStudentRequest request) {
        GetAllStudentResponse response = new GetAllStudentResponse();
        List<StudentType> studentTypeList = new ArrayList<>();
        List<Student> studentList = studentService.getAllStudent();
        for (Student student : studentList) {
            StudentType studentType = new StudentType();
            BeanUtils.copyProperties(student, studentType);
            studentTypeList.add(studentType);
        }
        response.getStudentType().addAll(studentTypeList);

        return response;

    }

    // liste des établissemnts
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllEstablishmentRequest")
    @ResponsePayload
    public GetAllEstablishmentResponse getAllEstablishment(@RequestPayload GetAllEstablishmentRequest request) {
        GetAllEstablishmentResponse response = new GetAllEstablishmentResponse();
        List<EstablishmentType> establishmentTypeList = new ArrayList<>();
        List<Establishment> establishmentList = establishmentService.getAllEstablishment();
        for (Establishment establishment: establishmentList) {
             EstablishmentType establishmentType = new EstablishmentType();
            BeanUtils.copyProperties(establishment, establishmentType);
            establishmentTypeList.add(establishmentType);
        }
        response.getEstablishmentType().addAll(establishmentTypeList);

        return response;

    }

    //liste des filières de l'UDs
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllSpecialityRequest")
    @ResponsePayload
    public GetAllSpecialityResponse getAllSpeciality(@RequestPayload GetAllSpecialityRequest request) {
        GetAllSpecialityResponse response = new GetAllSpecialityResponse();
        List<SpecialityType> specialityTypeList = new ArrayList<>();
        List<Speciality> specialityList = specialityService.getAllSpeciality();
        for (Speciality speciality: specialityList) {
            SpecialityType specialityType = new SpecialityType();
            BeanUtils.copyProperties(speciality, specialityType);
            specialityTypeList.add(specialityType);
        }
        response.getSpecialityType().addAll(specialityTypeList);

        return response;

    }

    //liste des filières d'une faculté
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getSpecialityForEstablishmentRequest")
    @ResponsePayload
    public GetSpecialityForEstablishmentResponse getAllSpecialityForEstablishment(@RequestPayload GetSpecialityForEstablishmentRequest request) {
        GetSpecialityForEstablishmentResponse response = new GetSpecialityForEstablishmentResponse();
        List<SpecialityType> specialityTypeList = new ArrayList<>();
        List<Speciality> specialityList = specialityService.getSpecialityForEstablishment(request.getEstablishmentId());
        for (Speciality speciality: specialityList) {
            SpecialityType specialityType = new SpecialityType();
            BeanUtils.copyProperties(speciality, specialityType);
            specialityTypeList.add(specialityType);
        }
        response.getSpecialityType().addAll(specialityTypeList);

        return response;

    }


    // liste du personal de l'uds
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPersonalSpecialityRequest")
    @ResponsePayload
    public GetAllPersonalSpecialityResponse getAllPersonalBySpeciality(@RequestPayload GetAllPersonalSpecialityRequest request) {
        GetAllPersonalSpecialityResponse response = new GetAllPersonalSpecialityResponse();
        List<PersonalType> personalTypeList = new ArrayList<>();
        List<Personal> personalList = personalService.getAllPersonalForSpeciality(request.getSpecialityId());
        for (Personal personal: personalList) {
            PersonalType personalType = new PersonalType();
            BeanUtils.copyProperties(personal, personalType);
            personalTypeList.add(personalType);
        }
        response.getPersonalType().addAll(personalTypeList);

        return response;

    }

    // Information d'un personal précis d'une filière
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPersonalByIdRequest")
    @ResponsePayload
    public GetPersonalByIdResponse getPersonalById(@RequestPayload GetPersonalByIdRequest request) {
        GetPersonalByIdResponse response = new GetPersonalByIdResponse();
        Personal personal = personalService.getInfoPersonal(request.getPersonalId());
        PersonalType personalType = new PersonalType();
        BeanUtils.copyProperties(personal, personalType);
        response.setPersonalType(personalType);
        return response;

    }

    // Ajout d'un Planing
@PayloadRoot(namespace = NAMESPACE_URI, localPart = "addPlaningRequest")
    @ResponsePayload
    public AddPlaningResponse addPlaning(@RequestPayload AddPlaningRequest  request) {
        AddPlaningResponse response = new AddPlaningResponse();
        PlaningType planingType = new PlaningType();
        ServiceStatus serviceStatus = new ServiceStatus();
        PlaningDto planingDto = new PlaningDto();
        planingDto.setDate(new Date(request.getDate().toGregorianCalendar().getTime().getTime()));
        planingDto.setStartTime(Time.valueOf(request.getStartTime().toGregorianCalendar().toZonedDateTime().toLocalTime()));
        planingDto.setEndTime(Time.valueOf(request.getEndTime().toGregorianCalendar().toZonedDateTime().toLocalTime()));
        planingDto.setLevelId(request.getLevelId());
        planingDto.setCourseId(request.getCourseId());
        planingDto.setSpecialityId(request.getSpecialityId());
        planingDto.setTeacherId(request.getTeacherId());
        Planing planing = planingService.addPlaning(planingDto);

        if (planing == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding planing");
        } else {

            BeanUtils.copyProperties(planing, planingType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setPlaningType(planingType);
        response.setServiceStatus(serviceStatus);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPlaningForWeekRequest")
    @ResponsePayload
    public GetPlaningForWeekResponse getPlaningForWeek(@RequestPayload GetPlaningForWeekRequest request) throws DatatypeConfigurationException {
        GetPlaningForWeekResponse response = new GetPlaningForWeekResponse();
        List<PlaningType> planingTypeList = new ArrayList<>();
        SearchPlaningDto searchPlaningDto = new SearchPlaningDto();
        searchPlaningDto.setSpecialityId(request.getSpecialityId());
        searchPlaningDto.setLevelId(request.getLevelId());
        searchPlaningDto.setStartDate(new Date(request.getStartDate().toGregorianCalendar().getTime().getTime()));
        searchPlaningDto.setEndDate(new Date(request.getEndDate().toGregorianCalendar().getTime().getTime()));
        List<Planing> planingList = planingService.getPlaningForWeek(searchPlaningDto);
        for (Planing planing: planingList) {
            PlaningType planingType = new PlaningType();
            BeanUtils.copyProperties(planing, planingType);
            planingTypeList.add(planingType);
        }
        response.getPlaningType().addAll(planingTypeList);

        return response;

    }


    //Ajout d'un étudiant
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addStudentRequest")
    @ResponsePayload
    public AddStudentResponse addStudent(@RequestPayload AddStudentRequest  request) {
        AddStudentResponse response = new AddStudentResponse();
        ParameterType parameterType = new ParameterType();
        ServiceStatus serviceStatus = new ServiceStatus();
        StudentDto studentDto = new StudentDto();
        studentDto.setName(request.getName());
        studentDto.setEmail(request.getEmail());
        studentDto.setCountry(request.getCountry());
        studentDto.setLevelId(request.getLevelId());
        studentDto.setSpecialityId(request.getSpecialityId());
        Parameter parameter = studentService.addStudent(studentDto);

        if (parameter == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding student");
        } else {

            BeanUtils.copyProperties(parameter, parameterType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setParameterType(parameterType);
        response.setServiceStatus(serviceStatus);
        return response;

    }

    //Ajout d'un personel
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addPersonalRequest")
    @ResponsePayload
    public AddPersonalResponse addPersonal(@RequestPayload AddPersonalRequest  request) {
        AddPersonalResponse response = new AddPersonalResponse();
        ParameterType parameterType = new ParameterType();
        ServiceStatus serviceStatus = new ServiceStatus();
        PersonalDto personalDto = new PersonalDto();
        personalDto.setName(request.getName());
        personalDto.setEmail(request.getEmail());
        personalDto.setCountry(request.getCountry());
        personalDto.setGrade(request.getGrade());
        personalDto.setFunction(request.getFunction());
        personalDto.setSpecialityId(request.getSpecialityId());
        personalDto.setRole(request.getRole());
        Parameter parameter = personalService.addPersonal(personalDto);

        if (parameter == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding student");
        } else {

            BeanUtils.copyProperties(parameter, parameterType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setParameterType(parameterType);
        response.setServicePersonalStatus(serviceStatus);
        return response;

    }



/*@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateCampusRequest")
    @ResponsePayload
    public UpdateCampusResponse updateCampus(@RequestPayload UpdateCampusRequest request) {
        UpdateCampusResponse response = new UpdateCampusResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        // 1. Find if movie available
        Campus campus = campusSoapService.showCampus();

        if(campus == null) {
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Movie = " + request.getName() + " not found");
        }else {

            // 2. Get updated campus information from the request
            campus.setName(request.getName());
            campus.setDescription(request.getDescription());
            // 3. update the movie in database

            boolean flag = service.updateEntity(movieFromDB);

            if(flag == false) {
                serviceStatus.setStatusCode("CONFLICT");
                serviceStatus.setMessage("Exception while updating Entity=" + request.getTitle());;
            }else {
                serviceStatus.setStatusCode("SUCCESS");
                serviceStatus.setMessage("Content updated Successfully");
            }


        }

        response.setServiceStatus(serviceStatus);
        return response;
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteMovieRequest")
    @ResponsePayload
    public DeleteCampusResponse deleteCampus(@RequestPayload DeleteCampusRequest request) {
        DeleteCampusResponse response = new DeleteCampusResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        boolean flag = campusSoapService.delete(request.getCampusId());

        if (!flag) {
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while deletint Entity id=" + request.getCampusId());
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Deleted Successfully");
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }*/


}
