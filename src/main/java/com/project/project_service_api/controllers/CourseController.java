package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.CourseDto;
import com.project.project_service_api.dao.entity.Courses;
import com.project.project_service_api.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class CourseController {

    private final CourseService courseService;

    @PostMapping(value = "/course", produces = "application/json")
    @PreAuthorize("hasAnyAuthority('PERSONAL', 'ADMIN')")
    public ResponseEntity<Courses> store(@RequestBody CourseDto courseDto){
        return ResponseEntity.ok(courseService.addCourse(courseDto));
    }
}
