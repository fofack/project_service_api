package com.project.project_service_api.controllers;

import com.project.project_service_api.dao.dto.RoleDto;
import com.project.project_service_api.dao.entity.Role;
import com.project.project_service_api.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/rest")
public class RoleController {

    private final RoleService roleService;

    @PostMapping(value = "/role", produces = "application/json")
    public ResponseEntity<Role> store(@RequestBody RoleDto roleDto){
        return ResponseEntity.ok(roleService.addRole(roleDto));
    }
}
