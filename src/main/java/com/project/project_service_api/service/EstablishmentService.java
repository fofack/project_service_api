package com.project.project_service_api.service;

import com.project.project_service_api.dao.dto.EstablishmentDto;
import com.project.project_service_api.dao.entity.Campus;
import com.project.project_service_api.dao.entity.Establishment;
import com.project.project_service_api.dao.repository.CampusRepository;
import com.project.project_service_api.dao.repository.EstablishmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class EstablishmentService {

    private final EstablishmentRepository establishmentRepository;
    private final CampusRepository campusRepository;

    public List<Establishment> getAllEstablishment(){
        return establishmentRepository.findAll();
    }

    public Establishment addEstablishment(EstablishmentDto establishmentDto){
        Campus campus = campusRepository.findById(establishmentDto.getCampusId()).get();
        Establishment establishment = new Establishment(null,establishmentDto.getName(),establishmentDto.getDescription(),campus);
        return establishmentRepository.save(establishment);
    }
}
