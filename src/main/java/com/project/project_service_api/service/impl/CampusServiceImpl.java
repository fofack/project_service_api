package com.project.project_service_api.service.impl;

import com.project.project_service_api.dao.dto.CampusDto;
import com.project.project_service_api.dao.entity.Campus;
import com.project.project_service_api.dao.repository.CampusRepository;
import com.project.project_service_api.service.CampusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class CampusServiceImpl implements CampusService {

    private final CampusRepository campusRepository;

    @Override
    public List<Campus> getAllCampus() {
        return campusRepository.findAll();
    }

    @Override
    public Campus createCampus(CampusDto campusDto) {
        Campus campus = new Campus(null, campusDto.getName(), campusDto.getDescription());
        return campusRepository.save(campus);
    }

    @Override
    public Campus updateCampus(CampusDto campusDto, Long id) {
        return null;
    }

    @Override
    public Campus showCampus(Long id) {
        return campusRepository.findById(id).get();
    }

    @Override
    public void delete(Long id) {

    }
}
