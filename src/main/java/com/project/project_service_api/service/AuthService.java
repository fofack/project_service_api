package com.project.project_service_api.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.project_service_api.dao.dto.ResetPasswordDto;
import com.project.project_service_api.dao.entity.User;
import com.project.project_service_api.dao.repository.UserRepository;
import com.project.project_service_api.utils.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class AuthService {
    private final UserService userService;
    private final UserRepository userRepository;

    public void refreshTokenOfToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String jwtAccessToken = "";
        String auhToken = request.getHeader(JwtUtil.AUTH_HEADER);
        if (auhToken != null && auhToken.startsWith(JwtUtil.PREFIX)){
            try{
                String jwt = auhToken.substring(JwtUtil.PREFIX.length());
                Algorithm algorithm =Algorithm.HMAC256(JwtUtil.SECRET);
                JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = jwtVerifier.verify(jwt);
                String username = decodedJWT.getSubject();

                User user = userService.findUserByUsername(username);
                jwtAccessToken = JWT.create()
                        .withSubject(user.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis()+JwtUtil.EXPIRE_REFRESH_TOKEN))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles",user.getRoles().stream().map(r ->r.getName()).collect(Collectors.toList()))
                        .sign(algorithm);

                Map<String, String> idToken = new HashMap<>();
                idToken.put("access_token",jwtAccessToken);
                idToken.put("refresh_token",jwt);
                response.setContentType("application/json");
                new ObjectMapper().writeValue(response.getOutputStream(),idToken);

            }
            catch (Exception e){
                response.setHeader("error-message",e.getMessage());
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
            }
        }else {
            throw new RuntimeException("RefreshToken require");
        }
    }

    public void updatePassword(ResetPasswordDto resetPasswordDto, String username) {
        userRepository.resetPassword(new BCryptPasswordEncoder().encode(resetPasswordDto.getPassword()), username);
    }
}
