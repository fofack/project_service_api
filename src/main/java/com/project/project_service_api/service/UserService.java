package com.project.project_service_api.service;

import com.project.project_service_api.dao.entity.User;
import com.project.project_service_api.dao.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
     private final UserRepository userRepository;

    public User findUserByUsername(String username){
        return userRepository.findByUserame(username);
    }

    public User addUser(User user){
        return userRepository.save(user);
    }

}
