package com.project.project_service_api.service;

import com.project.project_service_api.dao.dto.RoleDto;
import com.project.project_service_api.dao.entity.Role;
import com.project.project_service_api.dao.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;

    public Role addRole(RoleDto roleDto){
        Role role = new Role(null,roleDto.getName(),roleDto.getDescription());
        return roleRepository.save(role);
    }

    public Role addNewRole(Role role){
        return roleRepository.save(role);
    }

    public Role findRole(String role_name){
        return roleRepository.findByName(role_name);
    }
}
