package com.project.project_service_api.service;

import com.project.project_service_api.dao.dto.PlaningDto;
import com.project.project_service_api.dao.dto.SearchPlaningDto;
import com.project.project_service_api.dao.entity.Planing;
import com.project.project_service_api.dao.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Time;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PlaningService {

    private final PlaningRepository planingRepository;
    private final LevelRepository levelRepository;
    private final CourseRepository courseRepository;
    private final SpecialityRepository specialityRepository;
    private final PersonalRepository personalRepository;

    public Planing getPlaningForLevelAndSpeciality(Long levelId, Long specialityId){
        return planingRepository.getPlaningForLevelAndSpeciality(levelId, specialityId);
    }

    public Planing addPlaning(PlaningDto planingDto){
        Planing planing = new Planing();
        planing.setDate(planingDto.getDate());
        planing.setStartTime(Time.valueOf(planingDto.getStartTime().toLocalTime()));
        planing.setEndTime(Time.valueOf(planingDto.getEndTime().toLocalTime()));
        planing.setLevel(levelRepository.findById(planingDto.getLevelId()).get());
        planing.setCourses(courseRepository.findById(planingDto.getCourseId()).get());
        planing.setTeacherId(personalRepository.findById(planingDto.getTeacherId()).get());
        planing.setSpeciality(specialityRepository.findById(planingDto.getSpecialityId()).get());
        return planingRepository.save(planing);
    }

    public List<Planing> getPlaningForWeek(SearchPlaningDto searchPlaningDto){
        return planingRepository.findPlaningByWeek(searchPlaningDto.getLevelId(),
            searchPlaningDto.getSpecialityId(),searchPlaningDto.getStartDate(),searchPlaningDto.getEndDate());
    }
}
