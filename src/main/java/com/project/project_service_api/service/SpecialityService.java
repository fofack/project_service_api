package com.project.project_service_api.service;

import com.project.project_service_api.dao.dto.SpecialityDto;
import com.project.project_service_api.dao.entity.Establishment;
import com.project.project_service_api.dao.entity.Speciality;
import com.project.project_service_api.dao.repository.EstablishmentRepository;
import com.project.project_service_api.dao.repository.SpecialityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SpecialityService {

    private final SpecialityRepository specialityRepository;
    private final EstablishmentRepository establishmentRepository;

    public List<Speciality> getAllSpeciality(){
        return specialityRepository.findAll();
    }

    public List<Speciality> getSpecialityForEstablishment(Long establishmentId){
        return specialityRepository.findByEstablishmentId(establishmentId);
    }

    public Speciality addSpeciality(SpecialityDto specialityDto){
        Establishment establishment = establishmentRepository.findById(specialityDto.getEstablishmentId()).get();
        Speciality speciality = new Speciality(null,specialityDto.getName(),specialityDto.getDescription(),establishment);
        return specialityRepository.save(speciality);
    }
}
