package com.project.project_service_api.service;


import com.project.project_service_api.dao.dto.CampusDto;
import com.project.project_service_api.dao.entity.Campus;

import java.util.List;

public interface CampusService {

    public List<Campus> getAllCampus();
    public Campus createCampus(CampusDto campusDto);
    public Campus updateCampus (CampusDto campusDto, Long id);
    public Campus showCampus(Long id);
    public void delete (Long id);
}
