package com.project.project_service_api.service;


import com.project.project_service_api.dao.dto.PersonalDto;
import com.project.project_service_api.dao.dto.PhotoDto;
import com.project.project_service_api.dao.entity.Parameter;
import com.project.project_service_api.dao.entity.Personal;
import com.project.project_service_api.dao.entity.User;
import com.project.project_service_api.dao.repository.PersonalRepository;
import com.project.project_service_api.dao.repository.RoleRepository;
import com.project.project_service_api.dao.repository.SpecialityRepository;
import com.project.project_service_api.dao.repository.UserRepository;
import com.project.project_service_api.utils.EnvUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class PersonalService {

    private final PersonalRepository personalRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final SpecialityRepository specialityRepository;

    public List<Personal> getAllPersonalForSpeciality(Long specialityId) {
        return personalRepository.getAllPersonalForSpeciality(specialityId);
    }

    public Personal getInfoPersonal(Long id) {
        return personalRepository.findById(id).get();
    }

    public Parameter addPersonal(PersonalDto personalDto) {
        String year = String.valueOf(LocalDateTime.now().getYear());
        String time = String.valueOf(LocalDateTime.now().getNano());
        String minute = String.valueOf(LocalDateTime.now().getSecond());
        User user = new User();
        String username = personalDto.getName().substring(personalDto.getName().length() - 1) + time.substring(time.length() - 1)+personalDto.getName().substring(personalDto.getName().length() - 2);
        String password = minute+personalDto.getName().substring(personalDto.getName().length() - 2) + time.substring(time.length() - 2);
        String registrationNumber = "CM-UDS-PERS_" + time.substring(time.length() - 4) + minute;
        Personal personal = new Personal();
        personal.setRegistrationNumber(registrationNumber);
        personal.setName(personalDto.getName());
        personal.setEmail(personalDto.getEmail());
        personal.setGrade(personalDto.getGrade());
        personal.setFunction(personalDto.getFunction());
        personal.setCountry(personalDto.getCountry());
        personal.setUsername(username);
        personal.setSpeciality(specialityRepository.findById(personalDto.getSpecialityId()).get());
        Personal newPersonal = personalRepository.save(personal);
        user.setUsername(newPersonal.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(password));
        user.setPersonal(personal);
        user.setStudent(null);
        userRepository.save(user);
        user.getRoles().add(roleRepository.findByName(personalDto.getRole()));
        Parameter parameter = new Parameter(user.getUsername(), password);
        return parameter;
    }


    public String updatePhoto(MultipartFile photo, String username) throws IOException {
        Path currentPath = Paths.get("");
        Path absolutePath = currentPath.toAbsolutePath();
        var photo_path = absolutePath + "/src/main/resources/static/photos/personals/";
        byte[] bytes = photo.getBytes();
        Path path = Paths.get(photo_path +photo.getOriginalFilename());
        Files.write(path, bytes);
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/src/main/resources/static/photos/personals/")
                .path(photo.getOriginalFilename())
                .toUriString();
        personalRepository.updatePhoto(fileDownloadUri, username);
        return fileDownloadUri;
    }

    public Personal register(Personal personal){
        personalRepository.save(personal);
        return null;
    }
}