package com.project.project_service_api.service;

import com.project.project_service_api.dao.dto.CourseDto;
import com.project.project_service_api.dao.entity.Courses;
import com.project.project_service_api.dao.repository.CourseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;

    public Courses addCourse(CourseDto courseDto){
        Courses courses = new Courses(null, courseDto.getCode(), courseDto.getTitle());
        return courseRepository.save(courses);
    }
}
