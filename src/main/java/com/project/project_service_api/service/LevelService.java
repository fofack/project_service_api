package com.project.project_service_api.service;

import com.project.project_service_api.dao.dto.LevelDto;
import com.project.project_service_api.dao.entity.Level;
import com.project.project_service_api.dao.entity.Speciality;
import com.project.project_service_api.dao.repository.LevelRepository;
import com.project.project_service_api.dao.repository.SpecialityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LevelService {

    private final LevelRepository levelRepository;
    private final SpecialityRepository specialityRepository;

    public Level addLevel(LevelDto levelDto){
        Speciality speciality = specialityRepository.findById(levelDto.getSpecialityId()).get();
        Level level = new Level();
        level.setName(levelDto.getName());
        level.setDescription(levelDto.getDescription());
        level.setSpeciality(speciality);
        return levelRepository.save(level);
    }
}
