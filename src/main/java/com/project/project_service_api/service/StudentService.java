package com.project.project_service_api.service;

import com.project.project_service_api.dao.dto.StudentDto;
import com.project.project_service_api.dao.entity.Parameter;
import com.project.project_service_api.dao.entity.Student;
import com.project.project_service_api.dao.entity.User;
import com.project.project_service_api.dao.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final SpecialityRepository specialityRepository;
    private final LevelRepository levelRepository;
    private final EstablishmentRepository establishmentRepository;

    public List<Student> getAllStudent(){
        return studentRepository.findAll();
    }
    public Parameter addStudent(StudentDto studentDto){
        String year = String.valueOf(LocalDateTime.now().getYear());
        String time = String.valueOf(LocalDateTime.now().getNano());
        String minute = String.valueOf(LocalDateTime.now().getSecond());
        User user = new User();
        String username = studentDto.getName().substring(studentDto.getName().length() - 1) + time.substring(time.length() - 1)+studentDto.getName().substring(studentDto.getName().length() - 2);
        String password = minute+studentDto.getName().substring(studentDto.getName().length() - 2) + time.substring(time.length() - 2);
        String registrationNumber = "CM-UDS-"+ year.substring(year.length()-2)+"SCI"+ time.substring(time.length()-4);
        Student student = new Student();
        student.setRegistrationNumber(registrationNumber);
        student.setName(studentDto.getName());
        student.setEmail(studentDto.getEmail());
        student.setCountry(studentDto.getCountry());
        student.setUsername(username);
        student.setSpeciality(specialityRepository.findById(studentDto.getSpecialityId()).get());
        student.setLevel(levelRepository.findById(studentDto.getLevelId()).get());
        Student newStudent = studentRepository.save(student);
        user.setUsername(newStudent.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(password));
        user.setStudent(student);
        user.setPersonal(null);
        userRepository.save(user);
        user.getRoles().add(roleRepository.findByName("STUDENT"));
        Parameter parameter = new Parameter(user.getUsername(), password);
        return parameter;
    }

}
