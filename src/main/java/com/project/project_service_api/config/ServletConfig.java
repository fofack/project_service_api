/*
package com.project.project_service_api.config;

import com.project.project_service_api.controllers.*;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServletConfig {

    @Bean
    public ResourceConfig resourceConfig(){
        ResourceConfig jerseyServlet = new ResourceConfig();
 jerseyServlet.packages("com/project/project_service_api/controllers");

        jerseyServlet.register(com.project.project_service_api.controllers.CampusController.class);
        jerseyServlet.register(com.project.project_service_api.controllers.CourseController.class);
        jerseyServlet.register(com.project.project_service_api.controllers.EstablishmentController.class);
        jerseyServlet.register(com.project.project_service_api.controllers.LevelController.class);
        jerseyServlet.register(com.project.project_service_api.controllers.PersonalController.class);
        jerseyServlet.register(com.project.project_service_api.controllers.PlaningController.class);
        jerseyServlet.register(com.project.project_service_api.controllers.RoleController.class);
        jerseyServlet.register(com.project.project_service_api.controllers.SpecialityController.class);
        jerseyServlet.register(com.project.project_service_api.controllers.StudentController.class);
        jerseyServlet.register(com.project.project_service_api.utils.JwtAuthenticationFilter.class);
        jerseyServlet.register(com.project.project_service_api.utils.JwtAuthorizationFilter.class);
        jerseyServlet.register(com.project.project_service_api.config.SecurityConfig.class);
        return jerseyServlet;
    }
}
*/
