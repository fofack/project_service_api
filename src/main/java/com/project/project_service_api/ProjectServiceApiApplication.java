package com.project.project_service_api;

import com.project.project_service_api.dao.entity.Role;
import com.project.project_service_api.dao.entity.User;
import com.project.project_service_api.service.RoleService;
import com.project.project_service_api.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collections;
import java.util.Set;

@SpringBootApplication
public class ProjectServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectServiceApiApplication.class, args);
	}

	@Bean
	CommandLineRunner start(RoleService roleservice, UserService userService){
		return args -> {
			roleservice.addNewRole(new Role(null,"PERSONAL","destiner aux personals"));
			roleservice.addNewRole(new Role(null,"TEACHER","destiner aux teachers"));
			roleservice.addNewRole(new Role(null,"STUDENT","destiner aux students"));
			Role role = roleservice.addNewRole(new Role(null,"ADMIN","destiner aux admins"));
			userService.addUser(new User(null,"admin", new BCryptPasswordEncoder().encode("admin"),null,null, Collections.singleton(role)));
		};
	}
}
