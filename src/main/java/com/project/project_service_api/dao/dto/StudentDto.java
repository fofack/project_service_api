package com.project.project_service_api.dao.dto;

import lombok.Data;

@Data
public class StudentDto {
    private String name;
    private String email;
    private String country;
    private Long specialityId;
    private Long levelId;
}
