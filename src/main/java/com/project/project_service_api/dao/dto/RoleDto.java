package com.project.project_service_api.dao.dto;

import lombok.Data;

@Data
public class RoleDto {

    private String name;
    private String description;
}
