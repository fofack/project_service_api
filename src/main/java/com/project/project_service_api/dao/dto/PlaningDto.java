package com.project.project_service_api.dao.dto;

import lombok.Data;

import java.sql.Date;
import java.sql.Time;

@Data
public class PlaningDto {

    private Date date;
    private Time startTime;
    private Time endTime;
    private Long levelId;
    private Long courseId;
    private Long teacherId;
    private Long specialityId;
}
