package com.project.project_service_api.dao.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class PhotoDto {

    private MultipartFile photo;
}
