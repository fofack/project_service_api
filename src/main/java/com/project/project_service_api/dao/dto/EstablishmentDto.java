package com.project.project_service_api.dao.dto;

import lombok.Data;

@Data
public class EstablishmentDto {
    private String name;
    private String description;
    private Long campusId;
}
