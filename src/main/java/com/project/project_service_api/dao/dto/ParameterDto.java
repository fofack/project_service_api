package com.project.project_service_api.dao.dto;

import lombok.Data;

@Data
public class ParameterDto {
    private String username;
    private String password;
}
