package com.project.project_service_api.dao.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class SearchPlaningDto {
    private Long levelId;
    private Long specialityId;
    private Date startDate;
    private Date endDate;
}
