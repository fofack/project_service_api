package com.project.project_service_api.dao.dto;

import lombok.Data;

@Data
public class PersonalDto {

    private String name;
    private String email;
    private String function;
    private String grade;
    private String country;
    private Long specialityId;
    private String role;
}
