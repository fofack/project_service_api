package com.project.project_service_api.dao.dto;

import lombok.Data;

@Data
public class LevelDto {

    private String name;
    private String description;
    private Long specialityId;
}
