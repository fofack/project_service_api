package com.project.project_service_api.dao.dto;

import lombok.Data;

@Data
public class CourseDto {

    private String code;
    private String title;
}
