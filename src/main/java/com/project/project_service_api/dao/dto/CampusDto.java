package com.project.project_service_api.dao.dto;

import lombok.Data;
import org.hibernate.annotations.NotFound;

@Data
public class CampusDto {

    @NotFound
    private String name;

    private String description;
}
