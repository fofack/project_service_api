package com.project.project_service_api.dao.repository;

import com.project.project_service_api.dao.entity.Personal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

@Repository
public interface PersonalRepository extends JpaRepository<Personal, Long> {

    @Query(value = "select * from personals p where p.speciality_id =?1", nativeQuery = true)
    public List<Personal> getAllPersonalForSpeciality(@Param("specialityId") Long specialityId);

    @Transactional
    @Modifying
    @Query(value = "update personals set picture = ?1 where username = ?2", nativeQuery = true)
    public void updatePhoto(String file, String username);
}
