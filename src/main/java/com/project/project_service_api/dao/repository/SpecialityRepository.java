package com.project.project_service_api.dao.repository;

import com.project.project_service_api.dao.entity.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpecialityRepository extends JpaRepository<Speciality, Long> {

    @Query(value = "select * from specialities s where s.establishment_id =?1", nativeQuery = true)
    public List<Speciality> findByEstablishmentId(Long establishmentId);

    @Query(value = "select * from specialities s where s.id = ?1", nativeQuery = true)
    public Speciality findSpecialityById(Long id);
}
