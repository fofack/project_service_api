package com.project.project_service_api.dao.repository;

import com.project.project_service_api.dao.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RoleRepository extends JpaRepository<Role, Long> {

    @Query(value = "select * from roles s where s.name = ?1", nativeQuery = true)
    public Role findByName(String name);
}
