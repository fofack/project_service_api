package com.project.project_service_api.dao.repository;

import com.project.project_service_api.dao.entity.Planing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface PlaningRepository extends JpaRepository<Planing, Long> {

    @Query(value = "select * from planings p where p.level_id =?1 and p.speciality_id =?2", nativeQuery = true)
    public Planing getPlaningForLevelAndSpeciality(Long levelId, Long specialityId);

    @Query(value = "select * from planings p where p.level_id=?1 and p.speciality_id=?2 and p.date >=?3 and p.date <=?4",nativeQuery = true)
    public List<Planing> findPlaningByWeek(Long levelId, Long specialityId, Date startDate, Date endDate);
}
