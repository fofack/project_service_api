package com.project.project_service_api.dao.repository;

import com.project.project_service_api.dao.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select * from users u where u.username = ?1", nativeQuery = true)
    public User findByUserame(String username);

    @Transactional
    @Modifying
    @Query(value = "update users set password = :password where username = :username", nativeQuery = true)
    public void resetPassword(@Param("password") String password, @Param("username") String username);

}
