package com.project.project_service_api.dao.repository;


import com.project.project_service_api.dao.entity.Campus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CampusRepository extends JpaRepository<Campus, Long> {

    public Campus findByName(@Param("name") String name);

}
