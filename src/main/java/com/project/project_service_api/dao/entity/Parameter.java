package com.project.project_service_api.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Parameter {

    private String username;
    private String password;
}
