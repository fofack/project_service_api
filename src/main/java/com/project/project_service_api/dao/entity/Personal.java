package com.project.project_service_api.dao.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "personals")
@EqualsAndHashCode(of = "id")
public class Personal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "registration_number")
    private String registrationNumber;
    private String name;
    private String email;
    private String grade;
    private String function;
    private String country;
    private byte[] picture;
    private String username;

    @ManyToOne
    @JoinColumn(name = "speciality_id")
    private Speciality speciality;
}